55 minutes 12h30-13h25

## moment actus
- usurpation keepass.info
https://www.hfrance.fr/un-faux-site-keepass-utilise-google-ads-et-punycode-pour-diffuser-des-logiciels-malveillants.html
- un opérateur mobile tombe en Australie et emmène la moitié du pays avec lui
https://mamot.fr/@bohwaz/111371407066553573
- les profs ont aussi leur campagne pix !
https://eduscol.education.fr/3839/developpez-vos-competences-numeriques-avec-pix-edu

## Trame générale

- accueil 7 minutes
- atelier
- moment actus 3 minutes

## pour cette fois

- accueil 7 minutes
- présentations profs et atelier 10 minutes
- création de 5 groupes qu'on répartit sur des tables où on pose un feuille A3
- brainstorming de toutes les idées d'exploration de sujets pour ces ateliers sur un côté de la feuille 7 minutes
- 15 minutes lecture des 3 cartes et discussions autour de celles-ci
    - elles vous inspirent des questions ?
    - des idées ?
    - une carte vous intéresse plus qu'une autre ?
    - des expériences ?
    avec prises de note de l'autre côté de la feuille
- moment actus 3 minutes
